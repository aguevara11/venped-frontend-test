import React, { useState, useEffect } from "react";
import Products from "./ProductsTableItem";
import { connect } from "react-redux";
import { useTranslation } from "react-i18next";
import { PRODUCT_TABLE_HEADER } from "../../constants/constants";
const ProductsTable = ({ products, orderBy, setOrderBy }) => {
  const [t, i18n] = useTranslation("global");

  const productTableHeader = PRODUCT_TABLE_HEADER;
  const setNewOrder = (title, order) => {
    let newOrder = "";
    if (orderBy.order === "") {
      newOrder = order;
    } else {
      newOrder = orderBy.order === "desc" ? "asc" : "desc";
    }

    let newOrderBy = {
      order_by: title,
      order: newOrder,
    };
    setOrderBy(newOrderBy);
  };
  return (
    <div className="bg-white shadow-lg rounded-sm border border-slate-200 relative">
      <header className="px-5 py-4">
        <h2 className="font-semibold text-slate-800">
          {t("products")}{" "}
          <span className="text-slate-400 font-medium">
            {products.pagination.totalResults}
          </span>
        </h2>
      </header>
      <div>
        {/* Table */}
        {Object.keys(products).length ? (
          <div className="overflow-x-auto">
            <table className="table-auto w-full">
              {/* Table header */}
              <thead className="text-xs font-semibold uppercase text-slate-500 bg-slate-50 border-t border-b border-slate-200">
                <tr>
                  {productTableHeader.map((header) => {
                    return (
                      <th
                        onClick={() => {
                          setNewOrder(header.id, header.order);
                        }}
                        key={`header-${header.id}`}
                        className="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap"
                      >
                        <div className="font-semibold text-left">
                          {t(header.title)}
                        </div>
                      </th>
                    );
                  })}
                </tr>
              </thead>
              {/* Table body */}
              <tbody className="text-sm divide-y divide-slate-200">
                {products.results.map((product) => {
                  return (
                    <Products
                      key={product.id}
                      id={product.id}
                      product={product.title}
                      total={product.price}
                      stock={product.stock}
                      tax={product.tax}
                    />
                  );
                })}
              </tbody>
            </table>
          </div>
        ) : (
          "loading"
        )}
      </div>
    </div>
  );
};

const mapStateToProps = ({ products }) => ({
  products: products,
});

export default connect(mapStateToProps)(ProductsTable);
