import React from "react";
import { TAX_DATA } from "../../constants/constants";
import { useTranslation } from "react-i18next";

const ProductsTableItem = (props) => {
  const [t, i18n] = useTranslation("global");

  const getTaxFriendlyName = (id) => {
    return t(TAX_DATA.find((tax) => tax.id === id).text);
  };

  return (
    <tr>
      <td className="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
        <div className="font-medium text-sky-500">{props.product}</div>
      </td>
      <td className="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
        <div className="font-medium text-slate-500">{props.total}</div>
      </td>
      <td className="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
        <div className="font-medium text-slate-500">{props.stock}</div>
      </td>
      <td className="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
        <div className="font-medium text-sky-500">
          {getTaxFriendlyName(props.tax)}
        </div>
      </td>
    </tr>
  );
};

export default ProductsTableItem;
