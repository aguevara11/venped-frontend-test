import React from "react";
import NotFoundImage from "../images/404-illustration.svg";
import { useTranslation } from "react-i18next";

const NotFound = () => {
  const [t, i18n] = useTranslation("global");

  return (
    <div className="h-28 grid place-items-center h-screen">
      <div className="inline-flex mb-8">
        <img
          src={NotFoundImage}
          width="176"
          height="176"
          alt="404 illustration"
        />
      </div>
      <div className="mb-6">{t("not-found-text")}</div>
    </div>
  );
};

export default NotFound;
