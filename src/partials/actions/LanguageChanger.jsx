import React from "react";
import { useTranslation } from "react-i18next";

const LanguageChanger = () => {
  const [t, i18n] = useTranslation("global");

  return (
    <div className="flex items-center space-x-3">
      <button
        onClick={() => i18n.changeLanguage("es")}
        type="button"
        className="text-white bg-blue-500 hover:bg-blue-600 focus:ring-4 focus:outline-none focus:ring-cyan-300 dark:focus:ring-cyan-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2"
      >
        ES
      </button>
      <button
        onClick={() => i18n.changeLanguage("en")}
        type="button"
        className="text-white bg-blue-500 hover:bg-blue-600 focus:ring-4 focus:outline-none focus:ring-cyan-300 dark:focus:ring-cyan-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2"
      >
        EN
      </button>
    </div>
  );
};

export default LanguageChanger;
