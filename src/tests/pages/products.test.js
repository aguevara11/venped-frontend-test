import React from 'react'
import { render, screen } from '@testing-library/react';
import Products from '../../pages/Products'
import configureStore from 'redux-mock-store'
import { Provider } from 'react-redux'
import { BrowserRouter as Router } from 'react-router-dom';
import { products } from '../mockFiles/products';

const mockStore = configureStore([]);
const store = mockStore(products);

describe('Products', () => {
    it('Render Products', () => {
        render(
            <Router>
                <Provider store={store}>
                    <Products />
                </Provider>
            </Router>
        )
    })
})