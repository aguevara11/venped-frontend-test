import React from 'react'
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import LanguageChanger from '../../../partials/actions/LanguageChanger';
configure({ adapter: new Adapter() });

describe('LanguageChanger tests', () => {
    var component;

    component = shallow(<LanguageChanger />)

    it('Should render the component with the 2 buttons for the language', () => {
        expect(component.find('button')).toHaveLength(2);
    });
})