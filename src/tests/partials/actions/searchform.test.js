import React from 'react'
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import SearchForm from '../../../partials/actions/SearchForm';
configure({ adapter: new Adapter() });

describe('SearchForm tests', () => {
    var placeholder, handleSubmit, updateTitle, component;
    beforeEach(() => {
        placeholder = "Placeholder";
        handleSubmit = jest.fn();
        updateTitle = jest.fn();
    })
    component = shallow(<SearchForm
        placeholder={placeholder}
        handleSubmit={handleSubmit}
        updateTitle={updateTitle}
    />)

    it('Should render the component', () => {
        expect(component.find('input')).toHaveLength(1);
        expect(component.find('input').prop('placeholder'));
    });
})