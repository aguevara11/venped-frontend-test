export const products = {
    "products": {
        "results": [
            {
                "id": "89",
                "title": "Small Steel Keyboard",
                "price": 26.26,
                "tax": "es_general_21",
                "stock": 49
            },
            {
                "id": "128",
                "title": "Intelligent Steel Plate",
                "price": 12.96,
                "tax": "es_general_21",
                "stock": 17
            },
            {
                "id": "185",
                "title": "Small Steel Lamp",
                "price": 49.02,
                "tax": "es_general_21",
                "stock": 30
            },
            {
                "id": "198",
                "title": "Synergistic Steel Gloves",
                "price": 22.91,
                "tax": "es_general_21",
                "stock": 0
            },
            {
                "id": "204",
                "title": "Small Steel Lamp",
                "price": 47.31,
                "tax": "es_general_21",
                "stock": 48
            },
            {
                "id": "435",
                "title": "Gorgeous Steel Knife",
                "price": 48.95,
                "tax": "es_general_21",
                "stock": 48
            },
            {
                "id": "542",
                "title": "Intelligent Steel Watch",
                "price": 6,
                "tax": "es_general_21",
                "stock": 3
            },
            {
                "id": "621",
                "title": "Synergistic Steel Table",
                "price": 16.66,
                "tax": "es_general_21",
                "stock": 31
            },
            {
                "id": "656",
                "title": "Awesome Steel Wallet",
                "price": 20.91,
                "tax": "es_general_21",
                "stock": 8
            },
            {
                "id": "660",
                "title": "Synergistic Steel Plate",
                "price": 69.5,
                "tax": "es_general_21",
                "stock": 8
            },
            {
                "id": "733",
                "title": "Intelligent Steel Knife",
                "price": 52.12,
                "tax": "es_general_21",
                "stock": 40
            },
            {
                "id": "840",
                "title": "Synergistic Steel Lamp",
                "price": 24.52,
                "tax": "es_general_21",
                "stock": 18
            },
            {
                "id": "845",
                "title": "Mediocre Steel Clock",
                "price": 73.64,
                "tax": "es_general_21",
                "stock": 3
            },
            {
                "id": "878",
                "title": "Gorgeous Steel Table",
                "price": 98.97,
                "tax": "es_general_21",
                "stock": 9
            },
            {
                "id": "918",
                "title": "Aerodynamic Steel Bench",
                "price": 71.37,
                "tax": "es_general_21",
                "stock": 39
            }
        ],
        "pagination": {
            "totalResults": 0,
            "limitValue": 20,
            "totalPages": 0,
            "currentPage": 1,
            "nextPage": null,
            "prevPage": null,
            "firstPage": true,
            "lastPage": false,
            "outOfRange": true
        }
    }
}