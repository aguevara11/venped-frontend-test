import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { useTranslation } from "react-i18next";

const PaginationNumeric = ({ products, page, updatePage }) => {
  const handlePaganiation = (pageToMove) => {
    const currentPage = products.pagination.currentPage;
    const totalPages = products.pagination.totalPages;
    if (pageToMove !== currentPage && pageToMove <= totalPages) {
      updatePage(pageToMove);
    }
  };
  const [t, i18n] = useTranslation("global");

  return (
    <div>
      {Object.keys(products).length ? (
        <div>
          <div className="flex justify-center">
            <nav className="flex" role="navigation" aria-label="Navigation">
              <div
                onClick={() => handlePaganiation(products.pagination.prevPage)}
                className="mr-2"
              >
                <span
                  className={`
                  inline-flex 
                  items-center 
                  justify-center 
                  rounded leading-5 
                  px-2.5 
                  py-2 
                  bg-white 
                  border 
                  ${
                    !products.pagination.prevPage
                      ? "text-slate-300"
                      : "hover:bg-blue-500"
                  }
                `}
                >
                  <span className="sr-only">{t("previous")}</span>
                  <wbr />
                  <svg className="h-4 w-4 fill-current" viewBox="0 0 16 16">
                    <path d="M9.4 13.4l1.4-1.4-4-4 4-4-1.4-1.4L4 8z" />
                  </svg>
                </span>
              </div>

              <ul className="inline-flex text-sm font-medium -space-x-px shadow-sm">
                {[...Array(products.pagination.totalPages)].map((e, i) => {
                  return (
                    <li key={`pages-${i + 1}`}>
                      <a
                        className={`
                            inline-flex 
                            items-center 
                            justify-center 
                            leading-5 px-3.5 
                            py-2 bg-white  
                            border border-slate-200 
                            text-slate-600 
                            hover:text-white
                            hover:bg-blue-500
                            ${
                              products.pagination.currentPage === i + 1
                                ? "text-blue-500"
                                : ""
                            }
                          `}
                        onClick={() => handlePaganiation(i + 1)}
                      >
                        {i + 1}
                      </a>
                    </li>
                  );
                })}
              </ul>

              <div
                onClick={() => handlePaganiation(products.pagination.nextPage)}
                className="ml-2"
              >
                <a
                  className={`
                    inline-flex 
                    items-center 
                    justify-center 
                    rounded 
                    leading-5 
                    px-2.5 
                    py-2 
                    bg-white 
                    border 
                    border-slate-200 
                    text-slate-600 h
                    over:text-white shadow-sm
                    ${
                      !products.pagination.nextPage
                        ? "text-slate-300"
                        : "hover:bg-blue-500"
                    }
                `}
                >
                  <span className="sr-only">{t("next")}</span>
                  <wbr />
                  <svg className="h-4 w-4 fill-current" viewBox="0 0 16 16">
                    <path d="M6.6 13.4L5.2 12l4-4-4-4 1.4-1.4L12 8z" />
                  </svg>
                </a>
              </div>
            </nav>
          </div>
          <div className="flex justify-center">
            <div className="text-sm text-slate-500 text-center sm:text-left py-2">
              <span className="font-medium text-slate-600">1</span> {t("to")}{" "}
              <span className="font-medium text-slate-600">
                {products.pagination.limitValue}
              </span>{" "}
              {t("of")}{" "}
              <span className="font-medium text-slate-600">
                {products.pagination.totalResults}
              </span>{" "}
              {t("results")}
            </div>
          </div>
        </div>
      ) : null}
    </div>
  );
};

const mapStateToProps = ({ products }) => ({
  products: products,
});

export default connect(mapStateToProps)(PaginationNumeric);
