import { LOADING_PRODUCTS_SUCCESS, LOADING_PRODUCTS_ERROR, IS_LOADING_PRODUCTS } from './types'
import { fetchProducts } from '../../api/products'

export const GET = (variables) => async dispatch => {
    dispatch({
        type: IS_LOADING_PRODUCTS,
        payload: true
    });
    try {
        const response = await fetchProducts(variables)
        dispatch({
            type: LOADING_PRODUCTS_SUCCESS,
            payload: response.data.data.fetchProducts
        })
    } catch (error) {
        dispatch({
            type: LOADING_PRODUCTS_ERROR,
            payload: { error: 'Unexpected error' }
        })
        console.log('error')
    }
}