import { LOADING_PRODUCTS_SUCCESS, LOADING_PRODUCTS_ERROR, IS_LOADING_PRODUCTS } from '../actions/types'

const INITIAL_STATE = {
    isLoading: true,
    pagination: {},
    results: []
}
export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case IS_LOADING_PRODUCTS:
            return {
                ...state,
                isLoading: action.payload
            }
        case LOADING_PRODUCTS_SUCCESS:
            return {
                ...state,
                isLoading: false,
                results: action.payload.results,
                pagination: action.payload.pagination,
            }

        case LOADING_PRODUCTS_ERROR:
            return {
                ...state,
                isLoading: false,
                error: action.payload
            }

        default:
            return state
    }
}
