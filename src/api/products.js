import axios from 'axios';
import { print } from 'graphql';
import gql from 'graphql-tag'
import * as constants from '../constants/constants'

const GET_PRODUCTS = gql`query FetchProducts(
    $tax_filter: [String!],
    $title_filter: String,
    $order_by: String,
    $order: String,
    $page: Int!,
    $per_page: Int!) {
    fetchProducts {
      results(taxFilter: $tax_filter, titleFilter: $title_filter, orderBy: $order_by, order: $order, page: $page, perPage:
        $per_page) {
        id
        title
        price
        tax
        stock
      }
      pagination(taxFilter: $tax_filter, titleFilter: $title_filter, orderBy: $order_by, order: $order, page: $page, perPage:
        $per_page) {
        totalResults
        limitValue
        totalPages
        currentPage
        nextPage
        prevPage
        firstPage
        lastPage
        outOfRange
      }
    }
  }`
export const fetchProducts = async (variables) => {
    return await axios.post(constants.GRAPHQL_API, {
        query: print(GET_PRODUCTS),
        variables,
      })
}