export const GRAPHQL_API = 'http://vps-123eb2fc.vps.ovh.net/graphql';
export const TAX_DATA = [
    {
        id: "es_general_21",
        text: "direct-indirect",
    },
    {
        id: "es_reduced_10",
        text: "real-time",
    },
    {
        id: "es_super-reduced_4",
        text: "top-channels",
    },
    {
        id: "fr_general_20",
        text: "sales-refund",
    },
    {
        id: "fr_reduced_5.5",
        text: "last-order",
    },
]
export const PRODUCT_TABLE_HEADER = [
    {
        id: "title",
        order: "desc",
        title: "product",
    },
    {
        id: "price",
        order: "desc",
        title: "total",
    },
    {
        id: "stock",
        order: "desc",
        title: "stock",
    },
    {
        id: "tax",
        order: "desc",
        title: "tax",
    },
];