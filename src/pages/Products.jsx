import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { useDispatch } from "react-redux";
import { GET as getProducts } from "../redux/actions/products.js";
import Sidebar from "../partials/Sidebar";
import Header from "../partials/Header";
import SearchForm from "../partials/actions/SearchForm";
import FilterButton from "../components/DropdownFilter";
import ProductsTable from "../partials/products/ProductsTable";
import PaginationNumeric from "../components/PaginationNumeric";
import Loader from "../partials/Loader.jsx";
import NotFound from "../partials/NotFound.jsx";
import { useTranslation } from "react-i18next";

const Products = ({ products }) => {
  const [t, i18n] = useTranslation("global");
  const [sidebarOpen, setSidebarOpen] = useState(false);
  const [title, setTitle] = useState("");
  const [page, setPage] = useState(1);
  const [tax, setTax] = useState([]);
  const [orderBy, setOrderBy] = useState({ order_by: "price", order: "desc" });
  const dispatch = useDispatch();

  const handleSubmit = async () => {
    const variables = {
      tax_filter: tax,
      title_filter: title,
      order_by: orderBy.order_by,
      order: orderBy.order,
      page: page,
      per_page: 15,
    };

    dispatch(getProducts(variables));
  };

  useEffect(() => {
    handleSubmit();
  }, [page, tax, orderBy]);

  return (
    <div className="flex h-screen overflow-hidden">
      {/* Sidebar */}
      <Sidebar sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />

      {/* Content area */}
      <div className="relative flex flex-col flex-1 overflow-y-auto overflow-x-hidden">
        {/*  Site header */}
        <Header sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />

        <main>
          <div className="px-4 sm:px-6 lg:px-8 py-8 w-full max-w-9xl mx-auto">
            {/* Page header */}
            <div className="sm:flex sm:justify-between sm:items-center mb-5">
              {/* Left: Title */}
              <div className="mb-4 sm:mb-0">
                <h1 className="text-2xl md:text-3xl text-slate-800 font-bold">
                  {t("catalogue")}
                </h1>
              </div>
              {/* Right: Actions */}
              <div className="grid grid-flow-col sm:auto-cols-max justify-start sm:justify-end gap-2">
                {/* Search form */}
                <SearchForm
                  placeholder={t("search-by")}
                  handleSubmit={handleSubmit}
                  updateTitle={(e) => setTitle(e.target.value)}
                />
                {/* Filter button */}
                <FilterButton
                  align="right"
                  setTax={(newTax) => setTax(newTax)}
                  tax={tax}
                />
              </div>
            </div>

            {products.isLoading ? (
              <Loader />
            ) : (
              <div>
                {products.results.length ? (
                  <div>
                    <ProductsTable
                      orderBy={orderBy}
                      setOrderBy={(newOrderBy) => setOrderBy(newOrderBy)}
                    />
                    {/* Pagination */}
                    <div className="mt-8">
                      <PaginationNumeric
                        updatePage={(newPage) => setPage(newPage)}
                        page={page}
                      />
                    </div>
                  </div>
                ) : (
                  <NotFound />
                )}
                {/* Table */}
              </div>
            )}
          </div>
        </main>
      </div>
    </div>
  );
};

const mapStateToProps = ({ products }) => ({
  products: products,
});

export default connect(mapStateToProps)(Products);
