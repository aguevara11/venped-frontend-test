const { defaults } = require('jest-config');
module.exports = {
    moduleFileExtensions: [...defaults.moduleFileExtensions, 'js', 'jsx'],
    moduleNameMapper: {
        "^.+\\.svg$": "jest-svg-transformer",
    }
};