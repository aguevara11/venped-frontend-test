# Mosaic React

React landing page template.

## Project setup

```
npm i
```

### Compiles and hot-reloads for development

```
npm run dev
```

### Run test suit

```
npm test --watch
```

### Customize configuration

See [Configuration Reference](https://vitejs.dev/guide/).
